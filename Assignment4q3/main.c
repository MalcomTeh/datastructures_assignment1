#include <stdio.h>
#include <string.h>
#define SIZE 50
typedef struct
{
    int id;
    char name[50];
} Student;
void inputStud(Student *s, int size);
void printStud(Student *s, int size);
int removeStud(Student *s, int *size, char *target);
int main()
{
    Student s[SIZE];
    int size=0, choice;
    char target[80];
    int result;

    printf("Select one of the following options: \n");
    printf("1: inputStud()\n");
    printf("2: removeStud()\n");
    printf("3: printStud()\n");
    printf("4: exit()\n");
    do
    {
        printf("Enter your choice: \n");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            printf("Enter size: \n");
            scanf("%d", &size);
            printf("Enter %d students: \n", size);
            inputStud(s, size);
            break;
        case 2:
            printf("Enter name to be removed: \n");
            scanf("\n");
            gets(target);
            printf("removeStud(): ");
            result = removeStud(s, &size, target);
            if (result == 0)
                printf("Successfully removed\n");
            else if (result == 1)
                printf("Array is empty\n");
            else if (result == 2)
                printf("The target does not exist\n");
            else
                printf("An error has occurred\n");
            break;
        case 3:
            printStud(s, size);
            break;
        }
    }
    while (choice < 4);
    return 0;
}
void inputStud(Student *s, int size)
{
    int i = 0;
    int tempId;
    char dummychar;

    for(i = 0; i<size; i++)
    {
        printf("Student ID:\n");
        scanf(" %d",&tempId);
        (s+i)->id = (int)tempId;
        scanf("%c",&dummychar);
        printf("Student Name:\n");
        gets((s+i)->name);
    }
}
void printStud(Student *s, int size)
{
    int x;

    printf("The current student list:\n");
    for(x = 0; x < size; x ++)
    {
        printf("Student ID: %d Student Name: %s\n", (s+x)->id,(s+x)->name);
    }
}
int removeStud(Student *s, int *size, char *target)
{
    if(*size == 0)
    {
        return 1;
    }

    int x;
    int found = 0;
    for(x = 0; x<*size; x++)
    {
        if(found == 1)
        {
            if(x+1>*size)   //Finished shifting remaining element
            {
                return 0;
            }
            else
            {
                *(s+x) = *(s+x+1);
            }

        }
        if(strcmp(target,(s+x)->name) == 0)
        {
            if(x+1>*size)   //Target is last element, so dont iterate forward to get next char, instead just reduce size by 1
            {
                *size = (*size)-1;
                found = 1;
                return 0;
            }
            else
            {
                *(s+x) = *(s+x+1);
                found = 1;
            }
        }
    }
    if(found==1)
    {
        *size = (*size)-1;
        return 0;
    }
    return 2;

}
