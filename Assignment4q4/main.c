#include <stdio.h>
#include <string.h>
#define MAX 100
typedef struct {
   char name[20];
   char telno[20];
} PhoneBk;
void printPB(PhoneBk *p, int size);
int readin(PhoneBk *p);
void search(PhoneBk *p, int size, char *target);
int main()
{
   PhoneBk s[MAX];
   char t[20];
   int size=0, choice, dummychar;

   printf("Select one of the following options: \n");
   printf("1: readin()\n");
   printf("2: search()\n");
   printf("3: printPB()\n");
   printf("4: exit()\n");
   do {
      printf("Enter your choice: \n");
      scanf("%d", &choice);
      switch (choice) {
         case 1:
            scanf("%c", &dummychar);
            size = readin(s);
            break;
         case 2:
            scanf("%c", &dummychar);
            printf("Enter search name: \n");
            gets(t);
            search(s,size,t);
            break;
         case 3:
            printPB(s, size);
            break;
      }
   } while (choice < 4);
   return 0;
}
void printPB(PhoneBk *p, int size)
{
 int x = 0;
    if(size == 0)
    {
        printf("Empty phonebook");
    }
    printf("The phonebook list:\n");
    for(x = 0; x < size; x++)
    {
        printf("Name: %s\n", (p+x)->name);
        printf("Telno: %s\n", (p+x)->telno);
    }}
int readin(PhoneBk *p)
{
int counter = 0;
    char tempNumber[100];
    char tempName[100];
    char dummyChar;

    do
    {
        printf("Enter name:\n");
        gets(tempName);
        if((strcmp(tempName,"#") == 0))
        {
            break;
        }
        printf("Enter tel:\n");
        gets(tempNumber);
        strcpy((p+counter)->name,tempName);
        strcpy((p+counter)->telno,tempNumber);
        counter++;
    }
    while((strcmp(tempName,"#") != 0));
    return counter;}
void search(PhoneBk *p, int size, char *target)
{
 int x, found= 0;
    for(int x = 0; x < size; x++)
    {
        if(strcmp((p+x)->name,target)==0)
        {
            printf("Name = %s, Tel = %s\n",(p+x)->name, (p+x)->telno);
            found = 1;
        }
    }
    if(found == 0)
    {
        printf("Name not found!\n");
    }}
