#include <stdio.h>
#include <string.h>
#include <ctype.h>

void processString(char *str, int *totVowels, int *totDigits);

void cipher(char *s);
void decipher(char *s);

void maxCharToFront(char *str);

int countSubstring(char str[], char substr[]);

int main()
{
    /*  Question 1
    char str[50];
    int totVowels, totDigits;
    printf("Enter the string: \n");
    gets(str);
    processString(str, &totVowels, &totDigits);
    printf("Total vowels = %d\n", totVowels);
    printf("Total digits = %d\n", totDigits);
    */

    /*  Question 2

    char str[80];

    printf("Enter the string: \n");
    gets(str);
    printf("To cipher: %s -> ", str);
    cipher(str);
    printf("%s\n", str);
    printf("To decipher: %s -> ", str);
    decipher(str);
    printf("%s\n", str);

    /*  Question 3

    char str[80];

    printf("Enter a string: \n");
    gets(str);
    printf("maxCharToFront(): ");
    maxCharToFront(str);
    puts(str);
    */

    /*
    char str[80],substr[80];

    printf("Enter the string: \n");
    gets(str);
    printf("Enter the substring: \n");
    gets(substr);
    printf("countSubstring(): %d\n", countSubstring(str, substr));
*/
    return 0;
}
void processString(char *str, int *totVowels, int *totDigits)
{
    char vowelList[10]= {'a','e','i','o','u','A','E','I','O','U'};
    char numList[10]= {'0','1','2','3','4','5','6','7','8','9'};

    *totDigits = 0;
    *totVowels = 0;
    char *temp;
    for(temp = str; *temp != '\0'; temp++)
    {
        int x = 0;
        for(x=0; x<10; x++)
        {
            if(*temp == vowelList[x])       //Check for vowel
            {
                *totVowels+=1;
                x=10;
            }
            else if(*temp == numList[x])        //Check for num
            {
                *totDigits+=1;
                x=10;
            }
        }
    }
}

void cipher(char *s)
{
    char *temp;
    temp = s;
    while(*temp != '\0')
    {

        if((*s >= 65 && *s <91) || (*s > 96 && *s <123))
        {
            if(*s=='z')
            {
                *s = 'a';
            }
            else if(*s=='Z')
            {
                *s = 'A';
            }
            else
            {
                *s = *temp+1;
            }
        }
        temp++;
        s++;

    }
}

void decipher(char *s)
{
    char *temp;
    temp = s;
    while(*temp != '\0')
    {

        if((*s >= 65 && *s <91) || (*s > 96 && *s <123))
        {
            if(*s=='a')
            {
                *s = 'z';
            }
            else if(*s=='A')
            {
                *s = 'Z';
            }
            else
            {
                *s = *temp-1;
            }
        }
        temp++;
        s++;

    }
}

void maxCharToFront(char *str)
{
    //Check for blank string
    if(*str == '\0')
    {
        return;
    }

    char *firstChar = str;

    char *temp;
    temp = str;
    char *highestChar = str;
    int index = 0;
    //First find the first highestChar, store it as a pointer
    do
    {

        if(*highestChar< *temp && *highestChar != *temp)
        {
            highestChar = temp;
        }
        temp++;
    }
    while(*temp != '\0' );

    //Take out highest char, move chars before to cover out the removed highest char, put back highest char
    //Also checks if highest char is already at the front
    char tempHighestChar = *highestChar;
    char *currentChar = highestChar;
    if(highestChar == firstChar)
    {
    }
    else
    {
        do
        {
            highestChar--;
            *currentChar = *highestChar;
            currentChar--;

        }
        while(highestChar != firstChar );
        *currentChar = tempHighestChar;
    }
}

int countSubstring(char str[], char substr[])
{
    char *temp = str;
    char *subTemp = substr;
    int foundSubStrings = 0;

    do
    {
        if(*temp == *subTemp)
        {
            char *subTemp2 = subTemp;
            char *temp2 = temp;

            do
            {
                subTemp2++;
                temp2++;
                if(*subTemp2 == '\0')
                {
                    foundSubStrings += 1;
                }
                if(*temp2 != *subTemp2)
                {
                    break;
                }

            }
            while(*subTemp2 != '\0');
        }
        temp++;
    }
    while(*temp != '\0');

    return foundSubStrings;
}
