#include <stdio.h>
#include <stdlib.h>
#define INIT_VALUE 999

double computePay1(int noOfHours, int payRate);
void computePay2(int noOfHours, int payRate, double *grossPay);
const int OVERTIME_HOURS= 160;

int countEvenDigits1(int number);
void countEvenDigits2(int number, int *count);

int allEvenDigits1(int num);
void allEvenDigits2(int num, int *result);

int extEvenDigits1(int num);
void extEvenDigits2(int num, int *result);

int main()
{
    /*
    //**********************************************************************************
    //    ____    __
    //   / __ \  /_ |
    //  | |  | |  | |
    //  | |  | |  | |
    //  | |__| |  | |
    //   \___\_\  |_|
    //
    //**********************************************************************************
    int noOfHours, payRate;
    double grossPay;

    printf("Enter number of hours: \n");
    scanf("%d", &noOfHours);
    printf("Enter hourly pay rate: \n");
    scanf("%d", &payRate);
    printf("computePay1(): %.2f\n", computePay1(noOfHours, payRate));
    computePay2(noOfHours, payRate, &grossPay);
    printf("computePay2(): %.2f\n", grossPay);
    */

    /*
    //**********************************************************************************
    //    ____    ___
    //   / __ \  |__ \
    //  | |  | |    ) |
    //  | |  | |   / /
    //  | |__| |  / /_
    //   \___\_\ |____|
    //
    //**********************************************************************************

    int number, result;

    printf("Enter a number: \n");
    scanf("%d", &number);
    printf("countEvenDigits1(): %d\n", countEvenDigits1(number));
    countEvenDigits2(number, &result);
    printf("countEvenDigits2(): %d\n", result);
    */


    /*
    //**********************************************************************************
    //   ____    ____
    //  / __ \  |___ \
    // | |  | |   __) |
    // | |  | |  |__ <
    // | |__| |  ___) |
    //  \___\_\ |____/
    //
    //**********************************************************************************

    int number, p=999, result=999;
    printf("Enter a number: \n");
    scanf("%d", &number);
    p = allEvenDigits1(number);
    if (p == 1)
        printf("allEvenDigits1(): yes\n");
    else if (p == 0)
        printf("allEvenDigits1(): no\n");
    else
        printf("allEvenDigits1(): error\n");
    allEvenDigits2(number, &result);
    if (result == 1)
        printf("allEvenDigits2(): yes\n");
    else if (result == 0)
        printf("allEvenDigits2(): no\n");
    else
        printf("allEvenDigits2(): error\n");

    */

    //**********************************************************************************
    //  ____    _  _
    // / __ \  | || |
    //| |  | | | || |_
    //| |  | | |__   _|
    //| |__| |    | |
    // \___\_\    |_|
    //**********************************************************************************


    int number, result = INIT_VALUE;

    printf("Enter a number: \n");
    scanf("%d", &number);
    printf("extEvenDigits1(): %d\n", extEvenDigits1(number));
    extEvenDigits2(number, &result);
    printf("extEvenDigits2(): %d\n", result);
    return 0;


    return 0;
}

//**********************************************************************************
//    ____    __
//   / __ \  /_ |
//  | |  | |  | |
//  | |  | |  | |
//  | |__| |  | |
//   \___\_\  |_|
//
//**********************************************************************************

double computePay1(int noOfHours, int payRate)
{
    return (noOfHours<OVERTIME_HOURS)?(double)noOfHours * payRate:((double)OVERTIME_HOURS * payRate)+(1.5*payRate)*(noOfHours-OVERTIME_HOURS);

}
void computePay2(int noOfHours, int payRate, double *grossPay)
{
    *grossPay = (noOfHours<OVERTIME_HOURS)?(double)noOfHours * payRate:((double)OVERTIME_HOURS * payRate)+(1.5*payRate)*(noOfHours-OVERTIME_HOURS);
}

//**********************************************************************************
//    ____    ___
//   / __ \  |__ \
//  | |  | |    ) |
//  | |  | |   / /
//  | |__| |  / /_
//   \___\_\ |____|
//
//**********************************************************************************

int countEvenDigits1(int number)
{
    int numEven = 0;
    while(number>0)
    {
        if(number%2==0)
        {
            numEven += 1;
            number = number / 10;
        }
        else
        {
            number = number / 10;
        }
    }
    return numEven;
}
void countEvenDigits2(int number, int *count)
{
    int numEven = 0;
    while(number>0)
    {
        if(number%2==0)
        {
            numEven += 1;
            number = number / 10;
        }
        else
        {
            number = number / 10;
        }
    }
    *count = numEven;
}

//**********************************************************************************
//   ____    ____
//  / __ \  |___ \
// | |  | |   __) |
// | |  | |  |__ <
// | |__| |  ___) |
//  \___\_\ |____/
//
//**********************************************************************************

int allEvenDigits1(int num)
{
    return (num % 2 == 0)?1:0;
}
void allEvenDigits2(int num, int *result)
{
    *result = (num % 2 == 0)?1:0;
}

//**********************************************************************************
//  ____    _  _
// / __ \  | || |
//| |  | | | || |_
//| |  | | |__   _|
//| |__| |    | |
// \___\_\    |_|
//**********************************************************************************

int extEvenDigits1(int num)
{
    int count = 0;
    double evenNumber = 0;
    int isEven = 0; //0=False/1=True
    while(num>0)
    {
        if(num%2==0)
        {
            isEven = 1;
            int x = 0;
            int exponent = 1;
            for(x = 0; x<count; x++)    //I would have used Math.pow instead of this hack for loop
            {
                exponent = exponent * 10;
            }
            evenNumber += (num%10)*(exponent);
            count++;
            num = num /10;
        }
        else
        {
            num = num/10;
        }
    }
    return (isEven==0)?-1:evenNumber;
}
void extEvenDigits2(int num, int *result)
{
    int count = 0;
    double evenNumber = 0;
    int isEven = 0; //0=False/1=True
    while(num>0)
    {
        if(num%2==0)
        {
            isEven = 1;
            int x = 0;
            int exponent = 1;
            for(x = 0; x<count; x++)
            {
                exponent = exponent * 10;
            }
            evenNumber += (num%10)*(exponent);
            count++;
            num = num /10;
        }
        else
        {
            num = num/10;
        }
    }
    *result = (isEven == 0)?-1:evenNumber;
}
