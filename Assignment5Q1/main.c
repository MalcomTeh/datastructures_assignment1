#include <stdio.h>
int rCountEvenDigits1(int num);
void rCountEvenDigits2(int num, int *result);
int main()
{
    int number, result;

    printf("Enter the number: \n");
    scanf("%d", &number);
    printf("rCountEvenDigits1(): %d\n", rCountEvenDigits1(number));
    rCountEvenDigits2(number, &result);
    printf("rCountEvenDigits2(): %d\n", result);
    return 0;
}
int rCountEvenDigits1(int num)
{
    int x = 0;
    if(num > 0)
    {
        //Function to get first number
        int tempNum = num;
        tempNum = tempNum % 10;
        if(num%2 == 0 && tempNum !=0)
        {
            x++;
            x = x + rCountEvenDigits1(num/10);
        }
        else
        {
            x = x +rCountEvenDigits1(num/10);
        }
    }
    return x;

}
void rCountEvenDigits2(int num, int *result)
{
      *result = 0;
    if(num > 0)
    {
        if(num%2 == 0)
        {
            rCountEvenDigits2(num/10, result);
            *result = *result + 1;
        }
        else
        {
            rCountEvenDigits2(num/10, result);
        }
    }
}
