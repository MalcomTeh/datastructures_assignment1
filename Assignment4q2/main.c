#include <stdio.h>
typedef struct
{
    char source;
    char code;
} Rule;
void createTable(Rule *table, int *size);
void printTable(Rule *table, int size);
void encodeChar(Rule *table, char *s, char *t, int size);
int main()
{
    char s[80],t[80], dummychar;
    int size, choice;
    Rule table[100];

    printf("Select one of the following options:\n");
    printf("1: createTable()\n");
    printf("2: printTable()\n");
    printf("3: encodeChar()\n");
    printf("4: exit()\n");
    do
    {
        printf("Enter your choice: \n");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            printf("createTable(): \n");
            createTable(table, &size);
            break;
        case 2:
            printf("printTable(): \n");
            printTable(table, size);
            break;
        case 3:
            scanf("%c",&dummychar);
            printf("Source string: \n");
            gets(s);
            encodeChar(table,s,t,size);
            printf("Encoded string: %s\n", t);
            break;
        default:
            break;
        }
    }
    while (choice < 4);
    return 0;
}
void printTable(Rule *table, int size)
{
    int i;
    for (i=0; i<size; i++)
    {
        printf("%d: %c->%c\n", i+1, table->source, table->code);
        table++;
    }
}
void createTable(Rule *table, int *size)
{
    int i;
    char source, code;

    printf("Enter number of rules: \n");
    scanf("%d", size);
    for(i = 0; i<*size; i++)
    {
        printf("Enter rule %d:\n", i+1);
        printf("Enter source character:\n");
        scanf(" %c",&source);
        printf("Enter code character:\n");
        scanf(" %c",&code);
        table->source = source;
        table->code = code;
        table++;
    }
    table='\0';
}
void encodeChar(Rule *table, char *s, char *t, int size)
{
    int x = 0;
    if(size>0)
    {
        do
        {
            *(t+x) = *(s+x);
            char temp = s[x];
            int y = 0;
            do
            {
                if(temp == (table+y)->source)
                {
                    *(t+x) = (table+y)->code;
                }
                y++;
            }
            while(y < size);
            x++;
        }
        while(*(s+x) != '\0');
        *(t+(x+1)) = '\0';
    }
    else
    {
        do
        {
            *(t+x) = *(s+x);
            x++;
        }
        while(*(s+x) != '\0');
        *(t+(x+1)) = '\0';
    }
}
