#include <stdio.h>
int rStrLen(char *s);
int main()
{
 char str[80];
 printf("Enter the string: \n");
 gets(str);
 printf("rStrLen(): %d\n", rStrLen(str));
 return 0;
}
int rStrLen(char *s)
{
    int x = 0;
    if(*s != '\0'){
        x++;
        x = x + rStrLen(++s);
    }
    return x;
}
